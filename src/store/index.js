import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    visitedIdentities: []
  },
  mutations: {
    ADD_VISITED_IDENTITY(state, identity_name) {
      if (state.visitedIdentities.indexOf(identity_name) === -1) {
        state.visitedIdentities = [...state.visitedIdentities, identity_name];
      }
    },
    CLEAR_HISTORY(state, identity_name = null) {
      if (identity_name == null) {
        state.visitedIdentities = [];
      } else {
        let index = state.visitedIdentities.indexOf(identity_name);
        if (index !== -1) state.visitedIdentities.splice(index, 1);
      }
    }
  },
  modules: {},
  getters: {
    VISITED_IDENTITIES: state => state.visitedIdentities
  }
});

export default store;