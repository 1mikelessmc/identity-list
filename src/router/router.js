import Vue from 'vue'
import Router from 'vue-router'

import vIdentityList from '../components/v-identity-list'
import vIdentityInfo from '../components/v-identity-info'

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'list',
      component: vIdentityList,
      meta: {
        title: 'Поиск людей'
      }
    },
    {
      path: '/identity/:name',
      name: 'identity',
      component: vIdentityInfo,
      props: true,
      meta: {
        title: 'Информация о человеке'
      }
    }
  ]
});

export default router;